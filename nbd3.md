### Zadanie 1
#####Query

```
MATCH (a:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(movies) RETURN movies
```
![alt_tex](images/graph1.svg)
#####Response

```
movies
"{title:Cloud Atlas,tagline:Everything is connected,released:2012}"
"{title:V for Vendetta,tagline:Freedom! Forever!,released:2006}"
"{title:The Matrix Revolutions,tagline:Everything that has a beginning has an end,released:2003}"
"{title:The Matrix Reloaded,tagline:Free your mind,released:2003}"
"{title:The Matrix,tagline:Welcome to the Real World,released:1999}"
```
### Zadanie 2
#####Query

```
MATCH (p:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(movies)-[:DIRECTED]-(dir:Person) RETURN dir
```
![alt_tex](images/graph2.svg)

#####Response
```
dir
"{name:Tom Tykwer,born:1965}"
"{name:Lana Wachowski,born:1965}"
"{name:Lilly Wachowski,born:1967}"
"{name:James Marshall,born:1967}"
"{name:Lana Wachowski,born:1965}"
"{name:Lilly Wachowski,born:1967}"
"{name:Lana Wachowski,born:1965}"
"{name:Lilly Wachowski,born:1967}"
"{name:Lana Wachowski,born:1965}"
"{name:Lilly Wachowski,born:1967}"
```
### Zadanie 3
#####Query

```
MATCH (actor:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(movies)-[:ACTED_IN]-(other_actor:Person) RETURN other_actor
```
![alt_tex](images/graph3.svg)
#####Response

```
other_actor
"{name:Tom Hanks,born:1956}"
"{name:Jim Broadbent,born:1949}"
"{name:Halle Berry,born:1966}"
"{name:John Hurt,born:1940}"
"{name:Stephen Rea,born:1946}"
"{name:Natalie Portman,born:1981}"
"{name:Ben Miles,born:1967}"
"{name:Laurence Fishburne,born:1961}"
"{name:Carrie-Anne Moss,born:1967}"
"{name:Keanu Reeves,born:1964}"
"{name:Laurence Fishburne,born:1961}"
"{name:Carrie-Anne Moss,born:1967}"
"{name:Keanu Reeves,born:1964}"
"{name:Emil Eifrem,born:1978}"
"{name:Laurence Fishburne,born:1961}"
"{name:Carrie-Anne Moss,born:1967}"
"{name:Keanu Reeves,born:1964}"
```
### Zadanie 4
#####Query

```
MATCH (actor:Person)-[a:ACTED_IN]->(m)
WITH actor, count(a) as acted
WHERE acted > 0
RETURN actor, acted
```
![alt_tex](images/graph4.svg)
#####Response

```
actor,acted
"{name:Keanu Reeves,born:1964}",7
"{name:Carrie-Anne Moss,born:1967}",3
"{name:Laurence Fishburne,born:1961}",3
"{name:Hugo Weaving,born:1960}",5
"{name:Emil Eifrem,born:1978}",1
"{name:Charlize Theron,born:1975}",2
"{name:Al Pacino,born:1940}",1
"{name:Tom Cruise,born:1962}",3
"{name:Jack Nicholson,born:1937}",5
"{name:Demi Moore,born:1962}",1
"{name:Kevin Bacon,born:1958}",3
"{name:Kiefer Sutherland,born:1966}",2
"{name:Noah Wyle,born:1971}",1
"{name:Cuba Gooding Jr.,born:1968}",4
"{name:Kevin Pollak,born:1957}",1
"{name:J.T. Walsh,born:1943}",2
"{name:James Marshall,born:1967}",1
"{name:Christopher Guest,born:1948}",1
"{name:Aaron Sorkin,born:1961}",1
"{name:Kelly McGillis,born:1957}",1
"{name:Val Kilmer,born:1959}",1
"{name:Anthony Edwards,born:1962}",1
"{name:Tom Skerritt,born:1933}",1
"{name:Meg Ryan,born:1961}",5
"{name:Renee Zellweger,born:1969}",1
"{name:Kelly Preston,born:1962}",1
"{name:Jerry O'Connell,born:1974}",2
"{name:Jay Mohr,born:1970}",1
"{name:Bonnie Hunt,born:1961}",2
"{name:Regina King,born:1971}",1
"{name:Jonathan Lipnicki,born:1996}",1
"{name:River Phoenix,born:1970}",1
"{name:Corey Feldman,born:1971}",1
"{name:Wil Wheaton,born:1972}",1
"{name:John Cusack,born:1966}",1
"{name:Marshall Bell,born:1942}",2
"{name:Helen Hunt,born:1963}",3
"{name:Greg Kinnear,born:1963}",2
"{name:Annabella Sciorra,born:1960}",1
"{name:Max von Sydow,born:1929}",2
"{name:Werner Herzog,born:1942}",1
"{name:Robin Williams,born:1951}",3
"{name:Ethan Hawke,born:1970}",1
"{name:Rick Yune,born:1971}",2
"{name:James Cromwell,born:1940}",2
"{name:Parker Posey,born:1968}",1
"{name:Dave Chappelle,born:1973}",1
"{name:Steve Zahn,born:1967}",2
"{name:Tom Hanks,born:1956}",12
"{name:Rita Wilson,born:1956}",1
"{name:Bill Pullman,born:1953}",1
"{name:Victor Garber,born:1949}",1
"{name:Rosie O'Donnell,born:1962}",2
"{name:Nathan Lane,born:1956}",2
"{name:Billy Crystal,born:1948}",1
"{name:Carrie Fisher,born:1956}",1
"{name:Bruno Kirby,born:1949}",1
"{name:Liv Tyler,born:1977}",1
"{name:Brooke Langton,born:1970}",1
"{name:Gene Hackman,born:1930}",3
"{name:Orlando Jones,born:1968}",1
"{name:Christian Bale,born:1974}",1
"{name:Zach Grenier,born:1954}",2
"{name:Richard Harris,born:1930}",1
"{name:Clint Eastwood,born:1930}",1
"{name:Takeshi Kitano,born:1947}",1
"{name:Dina Meyer,born:1968}",1
"{name:Ice-T,born:1958}",1
"{name:Halle Berry,born:1966}",1
"{name:Jim Broadbent,born:1949}",1
"{name:Ian McKellen,born:1939}",1
"{name:Audrey Tautou,born:1976}",1
"{name:Paul Bettany,born:1971}",1
"{name:Natalie Portman,born:1981}",1
"{name:Stephen Rea,born:1946}",1
"{name:John Hurt,born:1940}",1
"{name:Ben Miles,born:1967}",3
"{name:Emile Hirsch,born:1985}",1
"{name:John Goodman,born:1960}",1
"{name:Susan Sarandon,born:1946}",1
"{name:Matthew Fox,born:1966}",1
"{name:Christina Ricci,born:1980}",1
"{name:Rain,born:1982}",2
{name:Naomie Harris},1
"{name:Michael Clarke Duncan,born:1957}",1
"{name:David Morse,born:1953}",1
"{name:Sam Rockwell,born:1968}",2
"{name:Gary Sinise,born:1955}",2
"{name:Patricia Clarkson,born:1959}",1
"{name:Frank Langella,born:1938}",1
"{name:Michael Sheen,born:1969}",1
"{name:Oliver Platt,born:1960}",2
"{name:Danny DeVito,born:1944}",2
"{name:John C. Reilly,born:1965}",1
"{name:Ed Harris,born:1950}",1
"{name:Bill Paxton,born:1955}",3
"{name:Philip Seymour Hoffman,born:1967}",2
"{name:Diane Keaton,born:1946}",1
"{name:Julia Roberts,born:1967}",1
"{name:Madonna,born:1954}",1
"{name:Geena Davis,born:1956}",1
"{name:Lori Petty,born:1963}",1
```
### Zadanie 5
#####Query

```
MATCH (n:Person)-[:DIRECTED]->(m)<-[:WROTE]-(n:Person) RETURN n,m
```
![alt_tex](images/graph5.svg)
#####Response

```
n,m
"{name:Lilly Wachowski,born:1967}","{title:Speed Racer,tagline:Speed has no limits,released:2008}"
"{name:Lana Wachowski,born:1965}","{title:Speed Racer,tagline:Speed has no limits,released:2008}"
"{name:Cameron Crowe,born:1957}","{title:Jerry Maguire,tagline:The rest of his life begins now.,released:2000}"
"{name:Nancy Meyers,born:1949}","{title:Something's Gotta Give,released:2003}"
```
### Zadanie 6
#####Query

```
MATCH (h:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(k:Person {name: "Keanu Reeves"}) RETURN m
```
![alt_tex](images/graph6.svg)
#####Response

```
m
"{title:The Matrix Revolutions,tagline:Everything that has a beginning has an end,released:2003}"
"{title:The Matrix Reloaded,tagline:Free your mind,released:2003}"
"{title:The Matrix,tagline:Welcome to the Real World,released:1999}"
```
### Zadanie 7
#####Query

```
CREATE (CaptainAmerica:Movie {title:'Captain America: The First Avenger', released:2011, tagline:' being Captain America comes at a price '})

CREATE (director:Person {name:"Joe Johnston", born:1950})
CREATE (markus:Person {name:"Christopher Markus", born:1970})
CREATE (stephen:Person {name:"Stephen McFeely", born:1969})
CREATE (chris:Person {name:"Chris Evans", born:1981})
CREATE (samuel:Person {name:"Samuel L. Jackson", born:1948})

CREATE
(chris)-[:ACTED_IN{roles:['Captain America', 'Steve Rogers']}]->(CaptainAmerica),
(samuel)-[:ACTED_IN{roles:['Nick Fury']}]->(CaptainAmerica),
(director)-[:DIRECTED]->(CaptainAmerica),
(markus)-[:WROTE]->(CaptainAmerica),
(stephen)-[:WROTE]->(CaptainAmerica)

WITH CaptainAmerica
MATCH (hugo:Person{name: "Hugo Weaving"})
CREATE (hugo)-[:ACTED_IN{roles:['Johann Schmidt','Red Skull']}]->(CaptainAmerica)

```
![alt_tex](images/graph7.svg)
#####Response

```
Added 6 labels, created 6 nodes, set 16 properties, created 6 relationships, completed after 6 ms.
```
### Zadanie 8
#####Query

```
MATCH p=(d:town {name:"Darjeeling"})-[*]->(s:peak{name:"Sandakphu"})
RETURN DISTINCT p

MATCH p =(d:town)-[*]->(s:peak)
WHERE d.name = 'Darjeeling' AND s.name = 'Sandakphu'
RETURN p
```
![alt_tex](images/graph8.svg)
#####Response

```
p
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]"
```
### Zadanie 9
#####Query

```
MATCH p=allShortestPaths((d:town {name:"Darjeeling"})-[*..5]->(s:peak{name:"Sandakphu"}))
RETURN DISTINCT p
```
![alt_tex](images/graph9.svg)
#####Response

```
p
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
```
### Zadanie 10
#####Query

```
MATCH p=(d:town {name:"Darjeeling"})-[*..3 {winter: "true"}]->(s:peak{name:"Sandakphu"})
RETURN DISTINCT p
```
![alt_tex](images/graph10.svg)
#####Response

```
p
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]"``
```
### Zadanie 11
#####Query

```
MATCH p=(d:town{name:"Darjeeling"})-[*]->(s:peak{name:"Sandakphu"})
RETURN p
ORDER BY length(p)
```
![alt_tex](images/graph11.svg)
#####Response

```
p,length(p)
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",3
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",3
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-BhikeBhanja,winter:true,distance:18,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",3
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Tumbling,winter:true,distance:10,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Gurdum,winter:true,distance:6,summer:true},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",4
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Sabargram,winter:true,distance:14,summer:true},{name:Sabargram,altitude:10000 ft},{name:Sabargram,altitude:10000 ft},{name:Rammam-Sabargram,winter:true,distance:15,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Tumbling,winter:true,distance:7,summer:false},{name:Tumbling},{name:Tumbling},{name:Tumbling-Gairibus,winter:true,distance:8,summer:true},{name:Gairibus},{name:Gairibus},{name:Gairibus-BhikeBhanja,winter:true,distance:10,summer:true},{name:BhikeBhanja},{name:BhikeBhanja},{name:BhikeBhanja-Sandakphu,winter:true,distance:4,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
"[{name:Darjeeling},{name:Darjeeling-Rimbik,winter:true,distance:15,summer:true},{name:Rimbik},{name:Rimbik},{name:Rimbik-Sirikhola,winter:true,distance:4,summer:true},{name:Sirikhola},{name:Sirikhola},{name:Sirikhola-Rammam,winter:true,distance:7,summer:true},{name:Rammam},{name:Rammam},{name:Rammam-Gurdum,winter:true,distance:10,summer:false},{name:Gurdum},{name:Gurdum},{name:Gurdum-Sandakphu,winter:true,distance:12,summer:true},{name:Sandakphu,altitude:12400 ft}]",5
```
### Zadanie 12
#####Query

```
MATCH p=(d:town {name:"Darjeeling"})-[r:twowheeler*..6 {summer: "true"}]->(anywhere}) RETURN p
```
#####Response

```
(no changes, no records)
```
### Zadanie 13
#####Query

```
MATCH p=(d:town {name:"Darjeeling"})-[r:twowheeler*..6 {summer: "true"}]->(anywhere) RETURN p
```
#####Response

```
(no changes, no records)
```
### Zadanie 14
#####Query

```
MATCH p=(d:town{name:"Darjeeling"})-[*]->(s:peak{name:"Sandakphu"})
RETURN p
ORDER BY length(p)
```
#####Response

```
Airport,total
ORD,48
DTW,25
IAH,23
LAX,22
DEN,18
SFO,14
AZO,13
IAD,12
BNA,12
MSP,9
MOT,8
EWR,7
SLC,5
HNL,5
FSD,5
MCO,4
PDX,4
BOS,4
DFW,4
SAN,4
BHM,4
CVG,4
ABQ,4
FAT,4
DLH,4
SEA,3
PHX,3
SBN,3
SNA,2
LAS,2
ATL,2
CMH,2
SJC,2
LGA,2
TUS,2
SAT,1
FLL,1
KOA,1
SMF,1
RIC,1
IND,1
MCI,1
MSN,1
BIL,1
ROC,1
CLT,1
DCA,1
PBI,1
MIA,1
MOB,1
```
### Zadanie 15
#####Query

```

```
#####Response

```
```
### Zadanie 16
#####Query

```

```
#####Response

```
```
### Zadanie 17
#####Query

```

```
#####Response

```
```
### Zadanie 18
#####Query

```

```
#####Response

```
```
### Zadanie 19
#####Query

```

```
#####Response

```
```
### Zadanie 20
#####Query

```
MATCH (f:Flight)-[:ORIGIN]-(a:Airport)
WITH f,a
MATCH (t:Ticket)-[:ASSIGN]-(f)-[:DESTINATION]-(d:Airport)
WITH t,a,d,f
CREATE (a)-[:connection{cprice: t.price, cclass: t.class, cdate: f.date}]->(d)

MATCH p = (a:Airport)-[*0..10]-(b:Airport)
WHERE a.name = "LAX" AND b.name="DAY"
RETURN p, reduce(dist=0, d in relationships(p) | dist + d.dprice) AS totalPrice
ORDER BY totalPrice ASC
LIMIT 1
```
#####Response

```
```

